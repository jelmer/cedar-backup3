# -*- coding: iso-8859-1 -*-
# vim: set ft=python ts=3 sw=3 expandtab:
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#              C E D A R
#          S O L U T I O N S       "Software done right."
#           S O F T W A R E
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Author   : Kenneth J. Pronovici <pronovic@ieee.org>
# Language : Python 3 (>= 3.4)
# Project  : Cedar Backup, release 3
# Purpose  : Provides location to maintain release information.
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

"""
Provides location to maintain version information.

Module Attributes
=================

Attributes:
   AUTHOR: Author of software
   EMAIL: Email address of author
   COPYRIGHT: Copyright date
   VERSION: Software version
   DATE: Software release date
   URL: URL of Cedar Backup webpage

:author: Kenneth J. Pronovici <pronovic@ieee.org>
"""

AUTHOR      = "Kenneth J. Pronovici"
EMAIL       = "pronovic@ieee.org"
COPYRIGHT   = "2004-2011,2013-2017"
VERSION     = "3.1.12"
DATE        = "12 Nov 2017"
URL         = "https://bitbucket.org/cedarsolutions/cedar-backup3"

