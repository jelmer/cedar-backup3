README for cedar-backup3

The accompanying documentation package (cedar-backup3-doc) contains all of the
end-user and library public interface documentation for Cedar Backup.  You
really should install cedar-backup3-doc and read through the end-user
documentation before using Cedar Backup for the first time.  

This package does not install a configuration file in /etc/cback3.conf.  It
does install all of the cron jobs, log rotation, etc. that you would expect,
but assumes that a user will fill in /etc/cback3.conf completely on their own.
This is consistent with the way cedar-backup2 has always been managed.

The sample configuration file in the documentation directory is not adapted to
Debian.  It just exists to provide a starting point (to provide an example of
appropriate syntax), and is not intended to represent real working
configuration.  In fact, this sample configuration file includes examples of
nearly every type of configuration section (for reference) but in a form and in
a combination that probably no one will ever use.  In particular, things like
override paths should not really ever be needed on a Debian system.

Upstream Cedar Backup uses cdrecord and mkisofs for writing to CD media,
because those are still the standard tools in the Linux world (for now).  Due
to some conflicts with the upstream developer for cdrecord and mkisofs, Debian
does not ship those tools.  Instead, Debian ships compatible tools called wodim
and genisoimage.  The Debian cedar-backup3 package uses the customization
features added in 2.19.5 to force Cedar Backup to use wodim and genisoimage
rather than cdrecord and mkisofs.  You can still use the command override
configuration to specify your own commands, if you want -- configuration takes
precedence over the Debian customization.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Wed, 29 Jul 2015 13:09:07 -0500

