#!/bin/sh

# Configure the tests to run 
TESTS="unittest"

# Run portions of the autopkgtest suite, with an autopkgtest-like environment
RESULT="succeeded"
for TEST in ${TESTS}; do

   # Create the $ADTTMP directory like autopkgtest
   export ADTTMP=`mktemp -d`
   if [ $? != 0 ]; then
      echo "${TEST} failed: failed to create \$ADTTMP"
      exit 1
   elif [ ! -d "${ADTTMP}" ]; then
      echo "${TEST} failed: could not create \$ADTTMP"
      exit 1
   fi

   # Create the $ADT_ARTIFACTS directory like autopkgtest
   export ADT_ARTIFACTS=`mktemp -d`
   if [ $? != 0 ]; then
      echo "${TEST} failed: failed to create \$ADT_ARTIFACTS"
      exit 1
   elif [ ! -d "${ADT_ARTIFACTS}" ]; then
      echo "${TEST} failed: could not create \$ADT_ARTIFACTS"
      exit 1
   fi

   # Execute the testcase
   TESTCASE="debian/tests/${TEST}"
   if [ ! -f "${TESTCASE}" ]; then
      echo "${TEST} failed: testcase does not exist"
      RESULT="failed"
   else
      sh ${TESTCASE} -r   # -r to run in "debian/rules" mode
      if [ $? != 0 ]; then
         echo "${TEST} failed: non-zero exit status"
         RESULT="failed"
      fi
   fi

   # Remove the temporary directories
   rm -rf ${ADTTMP}
   rm -rf ${ADT_ARTIFACTS}
done

# Exit with non-zero upon failure, to kill the build
if [ "${RESULT}" = "failed" ]; then
   exit 1
fi

